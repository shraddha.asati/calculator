import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators} from '@angular/forms'

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
 
  calculatorForm!:FormGroup;
  result!:number;
  constructor(private fb: FormBuilder) { }

  ngOnInit(): void {
    this.calculatorForm = this.fb.group({
      num1:['', Validators.required],
      num2:['', Validators.required],
      operator:['', Validators.required]

    });
    this.calculate()

  }

  calculate(){
    this.calculatorForm.valueChanges.subscribe(()=>{
      const num1= parseFloat(this.calculatorForm.value.num1);
      const num2= parseFloat(this.calculatorForm.value.num2);
      const operator= this.calculatorForm.value.operator;

switch (operator) {
  case '+':
    this.result= num1+num2; break;

    case '-':
      this.result= num1-num2; break;

      case '*':
        this.result= num1*num2; break;
        case '/':
          this.result= num1/num2; break;
}

    })
  }



}
